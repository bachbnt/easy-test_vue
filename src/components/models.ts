export enum ProductType {
  FLIGHT = 'Flight',
  HOTEL = 'Hotel',
}
export interface Banner {
  id: string;
  avatar?: string;
  product?: ProductType;
  departure?: string;
  arrival?: string;
  destination?: string;
  url?: string;
  dateFrom?: number;
  dateTo?: number;
  rank?: string;
  enabled?: boolean;
}

import { Notify } from 'quasar';

export const notifySuccess = (message: string) => {
  Notify.create({
    position: 'top-right',
    type: 'positive',
    message,
    timeout: 2000,
  });
};

export const notifyError = (message: string) => {
  Notify.create({
    position: 'top-right',
    type: 'negative',
    message,
    timeout: 2000,
  });
};

export const notifyWarning = (message: string) => {
  Notify.create({
    position: 'top-right',
    type: 'warning',
    message,
    timeout: 2000,
  });
};

export const notifyInfo = (message: string) => {
  Notify.create({
    position: 'top-right',
    type: 'info',
    message,
    timeout: 2000,
  });
};

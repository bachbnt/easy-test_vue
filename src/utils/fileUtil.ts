export const fileToBase64 = (file: File): Promise<string> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      if (typeof reader.result === 'string') {
        const image = new Image();

        image.onload = () => {
          const aspectRatio = image.width / image.height;
          const targetAspectRatio = 200 / 500;

          if (Math.abs(aspectRatio - targetAspectRatio) < 0.01) {
            resolve(
              `data:${file.type};base64,${
                (reader.result as string).split(',')[1]
              }`
            );
          } else {
            reject(new Error('Invalid aspect ratio'));
          }
        };

        image.onerror = (error) => {
          reject(error);
        };

        image.src = `data:${file.type};base64,${reader.result.split(',')[1]}`;
      } else {
        reject(new Error('Invalid file type'));
      }
    };

    reader.onerror = (error) => {
      reject(error);
    };

    reader.readAsDataURL(file);
  });
};

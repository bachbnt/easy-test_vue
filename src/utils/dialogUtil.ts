import { Dialog } from 'quasar';

export const showConfirmDialog = (
  message = 'Are you sure you want to do this?',
  onOk?: () => void
) => {
  return Dialog.create({
    title: 'Confirm',
    message: message,
    persistent: true,
    ok: {
      color: 'primary',
      label: 'Confirm',
    },
    cancel: {
      flat: true,
      color: 'black',
      label: 'Cancel',
    },
    class: 'q-pa-xs',
  }).onOk(() => {
    onOk?.();
  });
};

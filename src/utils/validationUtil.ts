import { isValidUrl } from './stringUtil';
import { isValidDate } from './timeUtil';

export const generateRequiredRule = (message = 'Required') => {
  return (val: string) => (val && val.length > 0) || message;
};

export const generateDateRule = (message = 'Invalid date') => {
  return (val: string) => isValidDate(val) || message;
};

export const generateUrlRule = (message = 'Invalid url') => {
  return (val: string) => isValidUrl(val) || message;
};

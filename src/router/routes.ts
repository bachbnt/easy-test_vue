import MainLayout from 'src/layouts/MainLayout.vue';
import AdBannerPage from 'src/pages/AdBannerPage.vue';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '',
    component: MainLayout,
    children: [
      { path: '', component: AdBannerPage },
      {
        path: '/advertisement-banners',
        component: AdBannerPage,
      },
      { path: '/general', component: () => import('pages/EmptyPage.vue') },
      { path: '/products', component: () => import('pages/EmptyPage.vue') },
      { path: '/packages', component: () => import('pages/EmptyPage.vue') },
      { path: '/suppliers', component: () => import('pages/EmptyPage.vue') },
      { path: '/languages', component: () => import('pages/EmptyPage.vue') },
      { path: '/currencies', component: () => import('pages/EmptyPage.vue') },
      {
        path: '/booking-questions',
        component: () => import('pages/EmptyPage.vue'),
      },
      { path: '/markups', component: () => import('pages/EmptyPage.vue') },
      { path: '/agency-types', component: () => import('pages/EmptyPage.vue') },
      { path: '/sub-agencies', component: () => import('pages/EmptyPage.vue') },
      { path: '/bookings', component: () => import('pages/EmptyPage.vue') },
      {
        path: '/flight-ticket-queuing',
        component: () => import('pages/EmptyPage.vue'),
      },

      { path: '/iam', component: () => import('pages/EmptyPage.vue') },
    ],
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;

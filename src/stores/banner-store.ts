import { defineStore } from 'pinia';
import { ProductType, Banner } from 'src/components/models';
import { DEFAULT_AVATAR } from 'src/constants/images';
import { notifyError, notifySuccess } from 'src/utils/notifyUtil';

export const useBannerStore = defineStore('counter', {
  state: () => ({
    banners: <Banner[]>[],
  }),
  actions: {
    fetchBanners() {
      const response = {
        statusCode: 200,
        data: [
          {
            id: '1',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '2',
            avatar: DEFAULT_AVATAR,
            product: ProductType.HOTEL,
            destination: 'Singapore',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '2',
            url: DEFAULT_AVATAR,
          },
          {
            id: '3',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '4',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '5',
            avatar: DEFAULT_AVATAR,
            product: ProductType.HOTEL,
            destination: 'Singapore',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '2',
            url: DEFAULT_AVATAR,
          },
          {
            id: '6',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '7',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '8',
            avatar: DEFAULT_AVATAR,
            product: ProductType.HOTEL,
            destination: 'Singapore',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '2',
            url: DEFAULT_AVATAR,
          },
          {
            id: '9',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '10',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
          {
            id: '11',
            avatar: DEFAULT_AVATAR,
            product: ProductType.HOTEL,
            destination: 'Singapore',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '2',
            url: DEFAULT_AVATAR,
          },
          {
            id: '12',
            avatar: DEFAULT_AVATAR,
            product: ProductType.FLIGHT,
            departure: 'HAN',
            arrival: 'SGN',
            dateFrom: 1752761600000,
            dateTo: 1755651200000,
            enabled: true,
            rank: '1',
            url: DEFAULT_AVATAR,
          },
        ],
      };
      this.banners = response.data;
    },
    addBanner(banner: Omit<Banner, 'id'>) {
      try {
        this.banners.unshift({ ...banner, id: Date.now().toString() });
        notifySuccess('Banner added successfully');
      } catch (error) {
        notifyError('Banner added failed');
      }
    },
    updateBanner(id: string, banner: Omit<Banner, 'id'>) {
      try {
        const index = this.banners.findIndex((b) => b.id === id);
        if (index !== -1) {
          this.banners[index] = { ...this.banners[index], ...banner };
        }
        notifySuccess('Banner updated successfully');
      } catch (error) {
        notifyError('Banner updated failed');
      }
    },
    deleteBanner(id: string) {
      try {
        const index = this.banners.findIndex((banner) => banner.id === id);
        if (index !== -1) {
          this.banners.splice(index, 1);
        }
        notifySuccess('Banner deleted successfully');
      } catch (error) {
        notifyError('Banner deleted failed');
      }
    },
  },
});
